import 'package:app_sitios_turisticos/models/TouristSiteImage.dart';

class TouristSite{

  double latitude;
  double longitude;
  double media_rate;
  String name;
  List<TouristSiteImage> touristicSiteImageList;
  TouristSite(this.latitude, this.longitude,  this.media_rate, this.name, this.touristicSiteImageList);

}