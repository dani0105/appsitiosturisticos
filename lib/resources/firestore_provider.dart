import 'package:app_sitios_turisticos/utils/strings.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class FirestoreProvider {
  Firestore _firestore = Firestore.instance;
  Stream<QuerySnapshot> getTouristicSitesList() => _firestore.collection(StringConstant.sitiosTuristicosCollection).snapshots();
  Stream<QuerySnapshot> getYoutubeVideos() => _firestore.collection(StringConstant.videosYoutubeCollection).snapshots();

}