import 'package:cloud_firestore/cloud_firestore.dart';
import 'firestore_provider.dart';

class Repository {
  final _firestoreProvider = FirestoreProvider();

  Stream<QuerySnapshot> getTouristicSitesList() => _firestoreProvider.getTouristicSitesList();
  Stream<QuerySnapshot> getYoutubeVideos() => _firestoreProvider.getYoutubeVideos();

}