import 'package:app_sitios_turisticos/ui/main_screen.dart';
import 'package:flutter/material.dart';
import 'blocs/blocs_provider.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocsProvider(
        child: MaterialApp(
          theme: ThemeData(
            accentColor: Colors.yellow,
            primaryColor: Colors.red,
          ),
          home: Scaffold(
            /*appBar: AppBar(
              title: Text(
                "Touristic sites",
                style: TextStyle(color: Colors.green),
              ),
              backgroundColor: Colors.white,
              elevation: 0.0,
            ),*/
            body: MainScreen(),
          ),
        ),
    );
  }
}
