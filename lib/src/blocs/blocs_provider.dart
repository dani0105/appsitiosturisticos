import 'package:app_sitios_turisticos/src/blocs/tourist_sites_bloc.dart';
import 'package:app_sitios_turisticos/src/blocs/youtube_videos_bloc.dart';
import 'package:flutter/material.dart';

class BlocsProvider extends InheritedWidget{
  final touristSitesBloc = TouristSitesBloc();
  final youtubeVideosBloc = YoutubeVideosBloc();

  BlocsProvider({Key key, Widget child}): super(key: key, child: child);

  bool updateShouldNotify(_) => true;

  static TouristSitesBloc ofForTouristSitesBloc(BuildContext context){
    return (context.inheritFromWidgetOfExactType(BlocsProvider) as BlocsProvider).touristSitesBloc;
  }

  static YoutubeVideosBloc ofForYoutubeVideosBloc(BuildContext context){
    return (context.inheritFromWidgetOfExactType(BlocsProvider) as BlocsProvider).youtubeVideosBloc;
  }



}