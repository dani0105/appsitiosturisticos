import 'package:app_sitios_turisticos/models/YoutubeVideo.dart';
import 'package:app_sitios_turisticos/resources/repository.dart';
import 'package:app_sitios_turisticos/utils/strings.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class YoutubeVideosBloc{
  final _repository = Repository();

  Stream<QuerySnapshot> getYoutubeVideosList(){
    return _repository.getYoutubeVideos();
  }

  void dispose() async {
  }

  List mapToList({DocumentSnapshot doc, List<DocumentSnapshot> docList}){
    List<YoutubeVideo> youtubeVideos = [];
    docList.forEach((document){

      String id_youtube = document.data[StringConstant.youtubeVideoIdField];
      String name = document.data[StringConstant.youtubeVideoNameField];

      YoutubeVideo youtubeVideo = YoutubeVideo(id_youtube, name);
      youtubeVideos.add(youtubeVideo);
    });

    return youtubeVideos;
  }

}