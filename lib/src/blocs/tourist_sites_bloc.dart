import 'package:app_sitios_turisticos/models/TouristSite.dart';
import 'package:app_sitios_turisticos/models/TouristSiteImage.dart';
import 'package:app_sitios_turisticos/resources/repository.dart';
import 'package:app_sitios_turisticos/utils/strings.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class TouristSitesBloc{
  final _repository = Repository();

  Stream<QuerySnapshot> getTouristicSitesList(){
    return _repository.getTouristicSitesList();
  }

  void dispose() async {

  }

  List mapToList({DocumentSnapshot doc, List<DocumentSnapshot> docList}){
      List<TouristSite> touristicSiteList = [];
      docList.forEach((document){

        double latitude = document.data[StringConstant.latitudeTouristSiteField];
        double longitude = document.data[StringConstant.longitudeTouristSiteField];
        double media_rate = document.data[StringConstant.mediaRateTouristSiteField];
        String name = document.data[StringConstant.nameTouristSiteField];

        List<TouristSiteImage> touristicSiteImageList = [];
        document.reference.collection("site_images").getDocuments().then((querySnaphot){
          querySnaphot.documents.forEach((documentSnapshot){
            String urlImage = documentSnapshot.data[StringConstant.urlImageTouristSiteField];
            TouristSiteImage touristSiteImage = TouristSiteImage(urlImage);
            touristicSiteImageList.add(touristSiteImage);
          });
        });

        TouristSite touristicSite = TouristSite(latitude, longitude,  media_rate, name, touristicSiteImageList);
        touristicSiteList.add(touristicSite);
      });

      return touristicSiteList;
  }

}

