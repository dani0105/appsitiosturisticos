import 'package:app_sitios_turisticos/models/YoutubeVideo.dart';
import 'package:app_sitios_turisticos/src/blocs/blocs_provider.dart';
import 'package:app_sitios_turisticos/src/blocs/youtube_videos_bloc.dart';
import 'package:app_sitios_turisticos/ui/styles.dart';
import 'package:app_sitios_turisticos/utils/strings.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:youtube_player/youtube_player.dart';
import 'package:flutter/material.dart';

class YoutubeVideosListTab extends StatefulWidget{
  @override
  _YoutubeVideosListState createState() {
    // TODO: implement createState
    return _YoutubeVideosListState();
  }
}

class _YoutubeVideosListState extends State<YoutubeVideosListTab>{
  YoutubeVideosBloc _bloc;

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment(0.0, 0.0),
      child: StreamBuilder(
          stream: _bloc.getYoutubeVideosList(),
          builder:(BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot){
            if (snapshot.hasData){
              List<DocumentSnapshot> docs = snapshot.data.documents;
              List<YoutubeVideo> sitesList = _bloc.mapToList(docList: docs);

              if(sitesList.isNotEmpty){
                return buildList(sitesList);
              } else {
                return Text(StringConstant.noVideos);
              }
            } else{
              return Text(StringConstant.noVideos);
            }
          }),
    );
  }

  ListView buildList(List<YoutubeVideo> videosList) {
    return ListView.separated(
        itemBuilder: (context, index){
          return Card(
            child: Container(
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(videosList[index].name, style: Styles.cardTitleText, textAlign: TextAlign.center,),
                  YoutubePlayer(
                    context: context,
                    source: videosList[index].id_youtube,
                    quality: YoutubeQuality.HD,
                    autoPlay: false,
                    startFullScreen: false,
                  )
                ],
              ),
            ),
          );
        },
        separatorBuilder: (BuildContext context, int index) => Divider(),
        itemCount: videosList.length);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _bloc = BlocsProvider.ofForYoutubeVideosBloc(context);
  }

}