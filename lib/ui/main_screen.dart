import 'package:app_sitios_turisticos/ui/tourist_sites_tab/tourist_sites_list_tab.dart';
import 'package:app_sitios_turisticos/ui/youtube_videos_tab/youtube_videos_list_tab.dart';
import 'package:app_sitios_turisticos/utils/strings.dart';
import 'package:flutter/material.dart';

class MainScreen extends StatefulWidget{

  @override
  MainScreenState createState() {
    // TODO: implement createState
    return MainScreenState();
  }

}

class MainScreenState extends State<MainScreen> with SingleTickerProviderStateMixin{

  TabController _tabController;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(
          StringConstant.touristSiteListTitle,
          style: TextStyle(
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.amber,
        elevation: 0.0,
        bottom: TabBar(
          controller: _tabController,
          tabs: <Widget>[
            Tab(text: StringConstant.touristSiteListTab),
            Tab(text: StringConstant.videosTab),
          ],
        ),
      ),

      body: TabBarView(
          controller: _tabController,
          children: <Widget>[
            TouristSitesListTab(),
            //VideosScreen(),
            YoutubeVideosListTab()
          ],
      ),
    );
  }

  @override
  void dispose() {
    _tabController.removeListener(_handleTabIndex);
    _tabController.dispose();
    super.dispose();
  }

  void _handleTabIndex(){
    setState((){});

  }

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this, initialIndex: 0);
    _tabController.addListener(_handleTabIndex);
  }


}
