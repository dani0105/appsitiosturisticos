import 'dart:async';

import 'package:app_sitios_turisticos/models/TouristSite.dart';
import 'package:app_sitios_turisticos/models/TouristSiteImage.dart';
import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class TouristSiteDetailScreen extends StatefulWidget {
  TouristSite touristSite;
  TouristSiteDetailScreen({Key key, @required this.touristSite})
      : super(key: key);

  @override
  _TouristeSiteDetailScreenState createState() =>
      _TouristeSiteDetailScreenState(this.touristSite);
}

class _TouristeSiteDetailScreenState extends State<TouristSiteDetailScreen> {
  Completer<GoogleMapController> _controller = Completer();
  TouristSite touristSite;
  MapType _currentMapType = MapType.normal;
  final Set<Marker> _markers = {};

  _TouristeSiteDetailScreenState(this.touristSite);

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }

  void _onMapTypeButtonPressed() {
    setState(() {
      _currentMapType = _currentMapType == MapType.normal
          ? MapType.satellite
          : MapType.normal;
    });
  }

  @override
  Widget build(BuildContext context) {

    _markers.add(Marker(
      markerId: MarkerId(touristSite.name),
      position: LatLng(touristSite.latitude, touristSite.longitude),
      infoWindow: InfoWindow(
        title: touristSite.name,
        snippet: 'Valoración promedio: ${touristSite.media_rate}',
      ),
      icon: BitmapDescriptor.defaultMarker,
    ));

    return Scaffold(
      appBar: AppBar(
        title: Text(touristSite.name),
        backgroundColor: Colors.amber,
      ),
      body: Padding(
          padding: EdgeInsets.all(8.0),
          child: ListView(
            children: <Widget>[
              /*Expanded(
              child: ListView(
                padding: EdgeInsets.symmetric(vertical: 8.0),
                children: touristSite.touristicSiteImageList.map((TouristSiteImage tsi){
                  return Image.network(tsi.url);
                }).toList()
              ),
            ),*/
              SizedBox(
                  height: 200.0,
                  width: double.infinity,
                  child: Carousel(
                    images: touristSite.touristicSiteImageList
                        .map((TouristSiteImage tsi) {
                      return NetworkImage(tsi.url);
                    }).toList(),
                    dotSize: 4.0,
                    dotSpacing: 15.0,
                    indicatorBgPadding: 5.0,
                    borderRadius: true,
                  )),
              Text("Ubicación en el mapa"),
              Stack(children: <Widget>[
                SizedBox(
                    height: 400.0,
                    width: double.infinity,
                    child: GoogleMap(
                      onMapCreated: _onMapCreated,
                      mapType: _currentMapType,
                      markers: _markers,
                      initialCameraPosition: CameraPosition(
                        target:
                            LatLng(touristSite.latitude, touristSite.longitude),
                        zoom: 15.0,
                      ),
                    )),
                FloatingActionButton(
                  onPressed: () => _onMapTypeButtonPressed,
                  materialTapTargetSize: MaterialTapTargetSize.padded,
                  backgroundColor: Colors.green,
                  child: const Icon(Icons.map, size: 36.0),
                )
              ])
            ],
          )
          //child: Image.network(touristSite.touristicSiteImageList[0].url),

          ),
    );
  }
}
