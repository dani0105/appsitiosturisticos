import 'package:app_sitios_turisticos/models/TouristSite.dart';
import 'package:app_sitios_turisticos/src/blocs/tourist_sites_bloc.dart';
import 'package:app_sitios_turisticos/src/blocs/blocs_provider.dart';
import 'package:app_sitios_turisticos/ui/tourist_sites_tab/tourist_site_detail_screen.dart';
import 'package:app_sitios_turisticos/utils/strings.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class TouristSitesListTab extends StatefulWidget{
  @override
  _TouristSitesListState createState() {
    // TODO: implement createState
    return _TouristSitesListState();
  }
}

class _TouristSitesListState extends State<TouristSitesListTab>{
  TouristSitesBloc _bloc;

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment(0.0, 0.0),
      child: StreamBuilder(
          stream: _bloc.getTouristicSitesList(),
          builder:(BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot){
            if (snapshot.hasData){
              List<DocumentSnapshot> docs = snapshot.data.documents;
              List<TouristSite> sitesList = _bloc.mapToList(docList: docs);

              if(sitesList.isNotEmpty){
                return buildList(sitesList);
              } else {
                return Text(StringConstant.noSites);
              }
            } else{
              return Text(StringConstant.noSites);
            }
          }),
    );
  }

  ListView buildList(List<TouristSite> sitesList) {
    return ListView.separated(
        itemBuilder: (context, index){
          return ListTile(
           title: Text(
             sitesList[index].name,
             style: TextStyle(
               fontWeight: FontWeight.bold,
             ),
           ),
            subtitle: Text(StringConstant.mediaRateText + ": " + sitesList[index].media_rate.toString()),
            onTap: (){
             Navigator.push(context,
             MaterialPageRoute(
                 builder: (context) => TouristSiteDetailScreen(touristSite: sitesList[index])
             )
             );
            },
          );
        },
        separatorBuilder: (BuildContext context, int index) => Divider(),
        itemCount: sitesList.length);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _bloc = BlocsProvider.ofForTouristSitesBloc(context);
  }

}