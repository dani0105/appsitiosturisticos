class StringConstant{

  static const String latitudeTouristSiteField = "latitude";
  static const String longitudeTouristSiteField = "longitude";
  static const String mediaRateTouristSiteField = "media_rate";
  static const String nameTouristSiteField = "name";
  static const String urlImageTouristSiteField = "url";
  static const String touristSiteListTitle = "Tourist sites";
  static const String touristSiteListTab = "Tourist sites";
  static const String videosTab = "Videos";
  static const String noSites = "No Sites";
  static const String noVideos = "No Videos";
  static const String mediaRateText = "Media rate";
  static const String videosYoutubeCollection = "VideosYoutube";
  static const String sitiosTuristicosCollection = "SitiosTuristicos";
  static const String youtubeVideoNameField = "name";
  static const String youtubeVideoIdField = "id_youtube";
}